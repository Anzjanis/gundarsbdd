@ICUZT16L12-141

Feature: UzdevumaPārsūtīšanasForma-ICUZT16L12-84
  Nepieciešams izveidot jaunu formu uzdevuma pārsūtīšanai.
  Piekļuve formai un tās elementiem tiek ierobežota ar lietotāju lomu tiesībām.

  Background: Es kā APAS lietotājs varu izveidot uzdevuma melnrakstu
    When Es izpildu createDraft metodi
    Then Es meklēju saņemtajā atbildē informāciju par ID "data/createTaskDraft/task/id"
    And Atbildes statuss ir "200"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/createTaskDraft/task/statusCode"
    And Atbildes satur "DRAFT"


  @ICUZT16L12-84-APITests
  Scenario Outline:  Es kā APAS lietotājs nevaru parsūtīt uzdevumu ar statusu "draft"
    When Es pārsutu uzdevumu lietotājam "A1" "mans komentars"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes statuss ir "<status>"
    And Atbildes satur "<sagaidamais rezultats>"

     Examples:
      |  status| informacijas vieta         | sagaidamais rezultats                            |
      |   200  |      errors/message        |Can't modify assignee if status != NEW or STARTED |


  @ICUZT16L12-84-APITests
  Scenario Outline:  Es kā APAS lietotājs varu parsūtīt uzdevumu ar statusu "jauns"
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<sutitaja uzdevuma status>"
    When Es pārsutu uzdevumu lietotājam "A1" "mans komentars"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 2>"
    And Atbildes satur "<sanemeja uzdevuma status>"
    And Atbildes statuss ir "<status>"
    When Es pieprasu informāciju par uzdevumu pēc ID "id" "comments"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/comments/description"
    Then Atbildes satur "<komentars>"

    Examples:
      |  status| informacijas vieta               | informacijas vieta 2              | sanemeja uzdevuma status | sutitaja uzdevuma status | komentars      |
      |   200  | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  |NEW                       | NEW                      | mans komentars |


  @ICUZT16L12-84-APITests
  Scenario Outline:  Es kā APAS lietotājs varu parsūtīt uzdevumu ar statusu "uzsākta izpilde"
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "NEW"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es pārsutu uzdevumu lietotājam "A1" "<komentars>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 2>"
    And Atbildes satur "<sanemeja uzdevuma status>"
    When Es pieprasu informāciju par uzdevumu pēc ID "id" "comments"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/comments/description"
    Then Atbildes satur "<komentars>"

    Examples:
      | informacijas vieta               | informacijas vieta 2              | sanemeja uzdevuma status | komentars      |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |


  @ICUZT16L12-84-APITests
  Scenario Outline:  Es kā APAS lietotājs varu parsūtīt uzdevumu pēc lauku rediģēšanas
    When Es atjaunoju melnraksta uzdevuma ipašību "<ipašības nosaukums>" "<ipašība>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<ipašība>"

    When Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "NEW"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es pārsutu uzdevumu lietotājam "A1" "<komentars>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 2>"
    And Atbildes satur "<sanemeja uzdevuma status>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "comments"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/comments/description"
    Then Atbildes satur "<komentars>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<ipašība>"

    Examples:
      | informacijas vieta               | informacijas vieta 2              | sanemeja uzdevuma status | komentars      | ipašības nosaukums | ipašība                      | informacijas vieta 3         |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |  title             | mans jauns title             | data/findTask/title          |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |  description       | mans jauns apraksts          | data/findTask/description    |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |  typeSchema        | mana jauna tipa shema        | data/findTask/typeSchema     |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |  typeCode          | mans jauns tipa kods         | data/findTask/typeCode       |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |  prioritySchema    | mana jauna prioritates shema | data/findTask/prioritySchema |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |  priorityCode      | HIGH                         | data/findTask/priorityCode   |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars |  deadline          | 2028-04-09T00:00             | data/findTask/deadline       |

  @ICUZT16L12-84-APITests
  Scenario Outline:  Es kā APAS lietotājs varu parsūtīt uzdevumu pēc pielikuma rediģēšanas
    When Es atjaunoju uzdevuma pielikuma lauku "<ipašības nosaukums>" ar informāciju "<ipašība>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "attachmentsFull"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<ipašība>"

    When Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "NEW"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es pārsutu uzdevumu lietotājam "A1" "<komentars>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 2>"
    And Atbildes satur "<sanemeja uzdevuma status>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "comments"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/comments/description"
    Then Atbildes satur "<komentars>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "attachmentsFull"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<ipašība>"

    Examples:
      | informacijas vieta               | informacijas vieta 2              | sanemeja uzdevuma status | komentars      | ipašības nosaukums | ipašība                | informacijas vieta 3                        |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars | description        | mans jauns apraksts    | data/findTask/attachments/description       |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars | fileType           | java                   | data/findTask/attachments/fileType          |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars | fileName           | mans jauns nosaukums   | data/findTask/attachments/fileName          |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars | UserInputName      | mans creator Name      | data/findTask/attachments/creator/name      |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars | UserInputLastName  | mans creator Lastnam   | data/findTask/attachments/creator/lastname  |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars | UserInputAuthority | mans creator authoryty | data/findTask/attachments/creator/authority |
      | data/createTask/task/statusCode  | data/forwardTask/task/statusCode  | NEW                      | mans komentars | UserInputId        | mans creator id        | data/findTask/attachments/creator/id        |


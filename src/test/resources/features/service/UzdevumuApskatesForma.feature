@ICUZT16L12-140

Feature: UzdevumuApskatesForma-ICUZT16L12-140
  Nepieciešams izveidot jaunu formu "Uzdevums" uzdevuma apskatei un apstrādei.
  Piekļuve formai un tās elementiem tiek ierobežota ar lietotāju lomu tiesībām (skatīt ICUZT16L12-88).

  Background: Es kā APAS lietotājs varu izveidot uzdevuma melnrakstu
    When Es izpildu createDraft metodi
    Then Es meklēju saņemtajā atbildē informāciju par ID "data/createTaskDraft/task/id"
    And Atbildes statuss ir "200"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/createTaskDraft/task/statusCode"
    And Atbildes satur "DRAFT"
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"

  @ICUZT16L12-140-APITests
  Scenario Outline: Es kā APAS lietotājs varu apskatīties uzdevuma statusu "NEW"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes statuss ir "200"
    And Atbildes satur "NEW"
    Examples:
    |IDprefix|
    |        |

  @ICUZT16L12-140-APITests
  Scenario Outline: Es kā APAS lietotājs varu apskatīties uzdevuma statusu "STARTED"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"
    And Atbildes statuss ir "200"
    Examples:
      |IDprefix|
      |        |
#
  @ICUZT16L12-140-APITests
  Scenario Outline: Es kā APAS lietotājs varu apskatīties uzdevuma statusu "DONE"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes statuss ir "200"
    And Atbildes satur "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "finishDate"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/finishDate"
    And Atbildes statuss ir "200"
    And Atbildes satur šodienas datumu
    Examples:
      |IDprefix|
      |        |

  @ICUZT16L12-140-APITests
  Scenario Outline: Es kā APAS lietotājs varu apskatīties uzdevuma vēsturi
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "history"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/history"
    And Atbildes statuss ir "200"
    And Atbildes satur "<informacijasMasivaVertiba>"
    Examples:
    |IDprefix| informacijasMasivaVertiba|
    |        | [{changeTypeCode:FINISHED,assignee:{authority:iecirknis,user:{id:123}},statusCode:DONE},{changeTypeCode:STARTED,assignee:{authority:iecirknis,user:{id:123}},statusCode:STARTED},{changeTypeCode:CREATED,assignee:{authority:iecirknis,user:{id:123}},statusCode:NEW}]|

  @ICUZT16L12-140-APITests
  Scenario Outline: Es kā APAS lietotājs varu atjaunot uzdevuma pielikumu un to aplūkot
    Given Es melnrakstam pievienoju pielikumu "<JaunaPielikumaInformacija>" "id" informācijas vieta "<ID info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<info Vieta>"
    Then Es atjaunoju uzdevuma pielikumu "description" ar informāciju "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbilde satures "<AttachmentInfoNew>" un informacija atrodas "<info Vieta>"
    And Atbildes satur "<AttachmentInfoNew>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacija ko atjaunot   | AttachmentInfoNew                                                        |IDprefix| informacija ko atjaunot| JaunaPielikumaInformacija| info Vieta| ID info Vieta |AttachmentInfoOld|
      |   200  |    atjaunotsPielikums     | [{description:atjaunotsPielikums},{description:test}] | | |jaunaPielikumaApraksts |data/findTask/attachments  | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |
     |   200  |    atjaunotsPielikums123    | [{description:atjaunotsPielikums123},{description:test}]| || jaunaPielikumaApraksts|data/findTask/attachments   | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |


  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pievienot uzdevumam komentāru un pēc tam to apskatīt
    Given Es uzdevumam pievienoju komentāru "<komentars>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Examples:
      |IDprefix| komentars|status |sagaidamaInformacija |
      |        |    komentars2      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars2}] |
      |        |    komentars ar atstarpi      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars ar atstarpi}] |
      |        |   123     |  200     | [{creator:{authority:test,name:test,lastname:test},description:123}] |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pievienot uzdevumam komentāru un tad sāk uzdevumu
    Given Es uzdevumam pievienoju komentāru "<komentars>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Examples:
      |IDprefix| komentars|status |sagaidamaInformacija |
      |        |    komentars2      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars2}] |
      |        |    komentars ar atstarpi      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars ar atstarpi}] |
      |        |   123     |  200     | [{creator:{authority:test,name:test,lastname:test},description:123}] |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pievienot uzdevumam komentāru un tad sāk uzdevumu un pabeidzu uzdevumu
    Given Es uzdevumam pievienoju komentāru "<komentars>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Examples:
      |IDprefix| komentars|status |sagaidamaInformacija |
      |        |    komentars2      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars2}] |
      |        |    komentars ar atstarpi      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars ar atstarpi}] |
      |        |   123     |  200     | [{creator:{authority:test,name:test,lastname:test},description:123}] |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pievienot uzdevumam komentāru un tad sāk uzdevumu un pabeidzu uzdevumu. DONE statusā vairs nav iespējams pievienot komentāru
    Given Es uzdevumam pievienoju komentāru "<komentars>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    And Atbildes statuss ir "<status>"
    Given Es uzdevumam pievienoju komentāru "komentars123456"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "comments"
    Then Es meklēju saņemtajā atbildē informācijas masīvu pēc "data/findTask/comments"
    And Atbildes satur "<sagaidamaInformacija>"
    Examples:
      |IDprefix| komentars|status |sagaidamaInformacija |
      |        |    komentars2      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars2}] |
      |        |    komentars ar atstarpi      |  200     | [{creator:{authority:test,name:test,lastname:test},description:komentars ar atstarpi}] |
      |        |   123     |  200     | [{creator:{authority:test,name:test,lastname:test},description:123}] |
@ICUZT16L12-140

Feature: UzdevumaPielikumaSkatisanasUnRedigesanasForma-ICUZT16L12-140
  Nepieciešams izveidot jaunu formu uzdevuma pielikuma apraksta un pielikuma nosaukuma ievadīšanai, rediģēšanai.

  Background: Es kā APAS lietotājs varu izveidot uzdevuma melnrakstu
    When Es izpildu createDraft metodi
    Then Es meklēju saņemtajā atbildē informāciju par ID "data/createTaskDraft/task/id"
    And Atbildes statuss ir "200"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/createTaskDraft/task/statusCode"
    And Atbildes satur "DRAFT"

  @ICUZT16L12-102-APITests
    Scenario Outline: Es kā APAS lietotājs varu pievienot pielikumu un to apskatīt DRAFT
    Given Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "createDate"
      When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
      And Atbildes statuss ir "<status>"
      And Atbilde satures "<info>" un informacija atrodas "<atrasanas>"
      Examples:
        |  status| informacija ko atjaunot|atrasanas| info|IDprefix|
        |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums},{description:test}] | |
        |   200  |    attachment: pievienotsJaunsPielikums123 |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums123},{description:test}] | |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu izdzēst pielikumu DRAFT
    Given Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "id" informācijas vieta "<info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<infoplace>"
    Given Es izdzēšu uzdevumu pielikumu "statusCode" kura vērtība "<boolean>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<title>" un informacija atrodas "<infoplace>"
    And Atbildes satur "<title>"
    Examples:
      | info Vieta                         |  status| informacija ko atjaunot                  |infoplace                         | IDprefix|title                 | AttachmentInfoOld                                  | boolean|
      |data/createAttachment/attachment/id |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments   |          |[{description:test}]|[{description:attachment: pievienotsJaunsPielikums},{description:test}]| true|
      |data/createAttachment/attachment/id  |   200  |    attachment: pievienotsPielikums123 |       data/findTask/attachments    |          | [{description:test}]|[{description:attachment: pievienotsPielikums123},{description:test}]|true |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu atjaunot pielikumu DRAFT
    Given Es melnrakstam pievienoju pielikumu "<JaunaPielikumaInformacija>" "id" informācijas vieta "<ID info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<info Vieta>"
    Then Es atjaunoju uzdevuma pielikumu "description" ar informāciju "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbilde satures "<AttachmentInfoNew>" un informacija atrodas "<info Vieta>"
    And Atbildes satur "<AttachmentInfoNew>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacija ko atjaunot   | AttachmentInfoNew      |IDprefix|informacija ko atjaunot| JaunaPielikumaInformacija| info Vieta| ID info Vieta |AttachmentInfoOld|
      |   200  |    atjaunotsPielikums     | [{description:atjaunotsPielikums},{description:test}]| | |jaunaPielikumaApraksts |data/findTask/attachments  | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |
      |   200  |    atjaunotsPielikums123    | [{description:atjaunotsPielikums123},{description:test}] | || jaunaPielikumaApraksts|data/findTask/attachments   | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |


  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pievienot pielikumu un to apskatīt STARTED
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"
    Then Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "createDate"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<info>" un informacija atrodas "<atrasanas>"
    Examples:
      |  status| informacija ko atjaunot|atrasanas| info|IDprefix|
      |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums},{description:test}] | |
      |   200  |    attachment: pievienotsJaunsPielikums123 |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums123},{description:test}] | |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu izdzēst pielikumu STARTED
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"
    Then Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "id" informācijas vieta "<info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<infoplace>"
    Given Es izdzēšu uzdevumu pielikumu "statusCode" kura vērtība "<boolean>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<title>" un informacija atrodas "<infoplace>"
    And Atbildes satur "<title>"
    Examples:
      | info Vieta                         |  status| informacija ko atjaunot                  |infoplace                         | IDprefix|title                 | AttachmentInfoOld                                  | boolean|
      |data/createAttachment/attachment/id |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments   |          |[{description:test}]|[{description:attachment: pievienotsJaunsPielikums},{description:test}]| true|
      |data/createAttachment/attachment/id  |   200  |    attachment: pievienotsPielikums123 |       data/findTask/attachments    |          | [{description:test}]|[{description:attachment: pievienotsPielikums123},{description:test}]|true |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu atjaunot pielikumu STARTED
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"
    Then Es melnrakstam pievienoju pielikumu "<JaunaPielikumaInformacija>" "id" informācijas vieta "<ID info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<info Vieta>"
    Then Es atjaunoju uzdevuma pielikumu "description" ar informāciju "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbilde satures "<AttachmentInfoNew>" un informacija atrodas "<info Vieta>"
    And Atbildes satur "<AttachmentInfoNew>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacija ko atjaunot   | AttachmentInfoNew      |IDprefix|informacija ko atjaunot| JaunaPielikumaInformacija| info Vieta| ID info Vieta |AttachmentInfoOld|
      |   200  |    atjaunotsPielikums     | [{description:atjaunotsPielikums},{description:test}]| | |jaunaPielikumaApraksts |data/findTask/attachments  | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |
      |   200  |    atjaunotsPielikums123    | [{description:atjaunotsPielikums123},{description:test}] | || jaunaPielikumaApraksts|data/findTask/attachments   | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pievienot pielikumu un to apskatīt NEW
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "NEW"
    Then Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "createDate"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<info>" un informacija atrodas "<atrasanas>"
    Examples:
      |  status| informacija ko atjaunot|atrasanas| info|IDprefix|
      |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums},{description:test}] | |
      |   200  |    attachment: pievienotsJaunsPielikums123 |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums123},{description:test}] | |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu izdzēst pielikumu NEW
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "NEW"
    Then Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "id" informācijas vieta "<info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<infoplace>"
    Given Es izdzēšu uzdevumu pielikumu "statusCode" kura vērtība "<boolean>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<title>" un informacija atrodas "<infoplace>"
    And Atbildes satur "<title>"
    Examples:
      | info Vieta                         |  status| informacija ko atjaunot                  |infoplace                         | IDprefix|title                 | AttachmentInfoOld                                  | boolean|
      |data/createAttachment/attachment/id |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments   |          |[{description:test}]|[{description:attachment: pievienotsJaunsPielikums},{description:test}]| true|
      |data/createAttachment/attachment/id  |   200  |    attachment: pievienotsPielikums123 |       data/findTask/attachments    |          | [{description:test}]|[{description:attachment: pievienotsPielikums123},{description:test}]|true |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs nevaru atjaunot pielikumu NEW
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "NEW"
    Then Es melnrakstam pievienoju pielikumu "<JaunaPielikumaInformacija>" "id" informācijas vieta "<ID info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<info Vieta>"
    Then Es atjaunoju uzdevuma pielikumu "description" ar informāciju "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbilde satures "<AttachmentInfoNew>" un informacija atrodas "<info Vieta>"
    And Atbildes satur "<AttachmentInfoNew>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacija ko atjaunot   | AttachmentInfoNew      |IDprefix|informacija ko atjaunot| JaunaPielikumaInformacija| info Vieta| ID info Vieta |AttachmentInfoOld|
      |   200  |    atjaunotsPielikums     | [{description:atjaunotsPielikums},{description:test}]| | |jaunaPielikumaApraksts |data/findTask/attachments  | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |
      |   200  |    atjaunotsPielikums123    | [{description:atjaunotsPielikums123},{description:test}] | || jaunaPielikumaApraksts|data/findTask/attachments   | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |



  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs nevaru pievienot pielikumu, bet varu to apskatīt DONE
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"
    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "DONE"
    Then Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<info>" un informacija atrodas "<atrasanas>"
    Examples:
      |  status| informacija ko atjaunot|atrasanas| info|IDprefix|
      |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments         | [{description:test}] | |
      |   200  |    attachment: pievienotsJaunsPielikums123 |       data/findTask/attachments         | [{description:test}] | |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs nevaru izdzēst pielikumu DONE, bet varu to apskatīt DONE
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"

    Then Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "id" informācijas vieta "data/createAttachment/attachment/id"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<infoplace>"

    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "DONE"
    Given Es izdzēšu uzdevumu pielikumu "statusCode" kura vērtība "Can't modify task"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<infoplace>"
    And Atbildes satur "<AttachmentInfoOld>"
    Examples:
     |  status| informacija ko atjaunot                  |infoplace                         | IDprefix               | AttachmentInfoOld                                  |
     |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments   |          |[{description:attachment: pievienotsJaunsPielikums},{description:test}]|
     |   200  |    attachment: pievienotsPielikums123 |       data/findTask/attachments    |          |[{description:attachment: pievienotsPielikums123},{description:test}]|

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs nevaru atjaunot pielikumu DONE, bet varu to apskatīt DONE
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"
    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "DONE"

    Then Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"

    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<info Vieta>"
    Then Es atjaunoju uzdevuma pielikumu "description" ar informāciju "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<info Vieta>"
    And Atbildes satur "<AttachmentInfoOld>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacija ko atjaunot   |       IDprefix|informacija ko atjaunot| info Vieta|AttachmentInfoOld |
      |   200  |    atjaunotsPielikums     |  |  |data/findTask/attachments  |[{description:test}] |
      |   200  |    atjaunotsPielikums123    |  ||data/findTask/attachments   | [{description:test}]|
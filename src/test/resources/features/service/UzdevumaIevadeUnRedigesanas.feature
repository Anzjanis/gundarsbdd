@ICUZT16L12-102

Feature: UzdevumaIevadeUnRedigesanasFormas-ICUZT16L12-36
  Nepieciešams izveidot jaunu formu uzdevuma ievadei un rediģēšanai.
  Piekļuve formai un tās elementiem tiek ierobežota ar lietotāju lomu tiesībām.

  Background: Es kā APAS lietotājs varu izveidot uzdevuma melnrakstu
    When Es izpildu createDraft metodi
    Then Es meklēju saņemtajā atbildē informāciju par ID "data/createTaskDraft/task/id"
    And Atbildes statuss ir "200"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/createTaskDraft/task/statusCode"
    And Atbildes satur "DRAFT"

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu meklēt melnrakstu uzdevumu
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "title"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<title>"
    And Atbildes statuss ir "<status>"
    Examples:
      | IDprefix |  status| informacijas vieta  | title |
      |          |   200  |data/findTask/title| TitleExample|
      |Test      |   200  |       data/findTask    |null |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā lietotājs varu atjaunot melnraksta title informāciju
    Given Es atjaunoju melnraksta title uzdevumu "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "title"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes statuss ir "<status>"
    And Atbildes satur "<atjaunota informacija>"
    Examples:
      |  status| informacija ko atjaunot|informacijas vieta| atjaunota informacija|IDprefix|
      |   200  |    title: TitleAtjaunots |       data/findTask/title         | title: TitleAtjaunots | |
      |   200  |    title: TitleAtjaunots123 |       data/findTask/title         | title: TitleAtjaunots123 | |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā lietotājs varu atjaunot melnraksta apraksta informāciju
    Given Es atjaunoju melnraksta aprakstu "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "description"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes statuss ir "<status>"
    And Atbildes satur "<atjaunota informacija>"
    Examples:
      |  status| informacija ko atjaunot|informacijas vieta| atjaunota informacija|IDprefix|
      |   200  |    description: descriptionAtjaunots |       data/findTask/description         | description: descriptionAtjaunots | |
      |   200  |    description: descriptionAtjaunots123 |       data/findTask/description         | description: descriptionAtjaunots123 | |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā lietotājs varu izdzēst uzdevuma melnrakstsu
    Given Es izdzēšu uzdevuma melnrakstu
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "title"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<title>"
    And Atbildes statuss ir "<status>"
    Examples:
      | IDprefix |  status| informacijas vieta  | title |
      |          |   200  |data/findTask| null|

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā lietotājs varu pievienot jaunu pielikumu
    Given Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "createDate"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<info>" un informacija atrodas "<atrasanas>"
    Examples:
      |  status| informacija ko atjaunot|atrasanas| info|IDprefix|
      |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums},{description:test}] | |
      |   200  |    attachment: pievienotsJaunsPielikums123 |       data/findTask/attachments         | [{description:attachment: pievienotsJaunsPielikums123},{description:test}] | |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu nomainīt uzdevuma statusu no DRAFT uz NEW
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    #Šajā gadijumā meklē uzdevumu un ne melnrakstu
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<title>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacijas vieta  | title |IDprefix |
      |   200  |data/findTask/statusCode| NEW| |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu sākt jaunu uzdevumu
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<newTitle>"
    And Atbildes statuss ir "<status>"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "<AtbildesInformacija>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<StartedTitle>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacijas vieta  | StartedTitle | IDprefix | AtbildesInformacija| newTitle|
      |   200  |data/findTask/statusCode| STARTED| |STARTED |   NEW   |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pabeigt sāktu uzdevumu
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<newTitle>"
    And Atbildes statuss ir "<status>"

    Then Es sāku šo uzdevumu "statusCode" kura vērtība "<StartedTitle>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<StartedTitle>"
    And Atbildes statuss ir "<status>"

    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "<AtbildesInformacija>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<DoneTitle>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacijas vieta     | DoneTitle | IDprefix | AtbildesInformacija| newTitle| StartedTitle|
      |   200  |data/findTask/statusCode| DONE      |          |DONE                |   NEW   |     STARTED |


  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu atgriezt uzdevumu New->Draft
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<newTitle>"
    And Atbildes statuss ir "<status>"

    Given Es atgriežu uzdevumu "statusCode" kura vērtība "<AtbildesInformacija>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<newStatus>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacijas vieta     | newStatus | IDprefix | AtbildesInformacija| newTitle|
      |   200  |data/findTask/statusCode| DRAFT      |          |DRAFT                |   NEW   |

  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu pārsūtīt uzdevumu
    Given Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<newTitle>"
    And Atbildes statuss ir "<status>"
    Then Es sāku šo uzdevumu "statusCode" kura vērtība "<AtbildesInformacija>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<AtbildesInformacija>"
    And Atbildes statuss ir "<status>"
    Given Es pārsūtu uzdevumu "statusCode" kura vērtība "<descriptionValue>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "<newStatus>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacijas vieta     | newStatus | IDprefix | AtbildesInformacija| newTitle| descriptionValue|
      |   200  |data/findTask/statusCode| NEW      |          |STARTED                |   NEW   |             NEW  |


  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu izdzēst uzdevuma pielikumu
    Given Es melnrakstam pievienoju pielikumu "<informacija ko atjaunot>" "id" informācijas vieta "<info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<infoplace>"
    Given Es izdzēšu uzdevumu pielikumu "statusCode" kura vērtība "<boolean>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<title>" un informacija atrodas "<infoplace>"
    And Atbildes satur "<title>"
    Examples:
      | info Vieta                         |  status| informacija ko atjaunot                  |infoplace                         | IDprefix|title                 | AttachmentInfoOld                                  | boolean|
      |data/createAttachment/attachment/id |   200  |    attachment: pievienotsJaunsPielikums |       data/findTask/attachments   |          |[{description:test}]|[{description:attachment: pievienotsJaunsPielikums},{description:test}]| true|
      |data/createAttachment/attachment/id  |   200  |    attachment: pievienotsPielikums123 |       data/findTask/attachments    |          | [{description:test}]|[{description:attachment: pievienotsPielikums123},{description:test}]|true |


  @ICUZT16L12-102-APITests
  Scenario Outline: Es kā APAS lietotājs varu atjaunot uzdevuma pielikumu un pēc tam to apskatīt
    Given Es melnrakstam pievienoju pielikumu "<JaunaPielikumaInformacija>" "id" informācijas vieta "<ID info Vieta>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbildes statuss ir "<status>"
    And Atbilde satures "<AttachmentInfoOld>" un informacija atrodas "<info Vieta>"
    Then Es atjaunoju uzdevuma pielikumu "description" ar informāciju "<informacija ko atjaunot>"
    When Es meklēju uzdevuma melnrakstu pēc "<IDprefix>" "attachments"
    And Atbilde satures "<AttachmentInfoNew>" un informacija atrodas "<info Vieta>"
    And Atbildes satur "<AttachmentInfoNew>"
    And Atbildes statuss ir "<status>"
    Examples:
      |  status| informacija ko atjaunot   | AttachmentInfoNew      |IDprefix|informacija ko atjaunot| JaunaPielikumaInformacija| info Vieta| ID info Vieta |AttachmentInfoOld|
      |   200  |    atjaunotsPielikums     | [{description:atjaunotsPielikums},{description:test}]| | |jaunaPielikumaApraksts |data/findTask/attachments  | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |
      |   200  |    atjaunotsPielikums123    | [{description:atjaunotsPielikums123},{description:test}] | || jaunaPielikumaApraksts|data/findTask/attachments   | data/createAttachment/attachment/id|[{description:jaunaPielikumaApraksts},{description:test}] |



  @ICUZT16L12-102-APITests
  Scenario Outline:  Es kā APAS lietotājs nevaru labot laukus, kad statuss ir NEW
    When Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "NEW"

    When Es atjaunoju melnraksta uzdevuma ipašību "<ipašības nosaukums>" "<ipašība>"
    Then Es pārliecinos ka ir atgriezts kļūdas ziņojums "Can't modify task with status <> draft"
    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<vecā īpašība>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<vecā īpašība>"

    Examples:
      | informacijas vieta                | ipašības nosaukums | ipašība                      | informacijas vieta 3         |vecā īpašība |
      | data/createTask/task/statusCode   |  title             | mans jauns title             | data/findTask/title          | TitleExample|
      | data/createTask/task/statusCode   |  description       | mans jauns apraksts          | data/findTask/description    |desct |
      | data/createTask/task/statusCode   |  typeSchema        | mana jauna tipa shema        | data/findTask/typeSchema     |null |
      | data/createTask/task/statusCode   |  typeCode          | mans jauns tipa kods         | data/findTask/typeCode       |TEST|
      | data/createTask/task/statusCode   |  prioritySchema    | mana jauna prioritates shema | data/findTask/prioritySchema |null |
      | data/createTask/task/statusCode   |  priorityCode      | HIGH                         | data/findTask/priorityCode   |LOW |



  @ICUZT16L12-102-APITests
  Scenario Outline:  Es kā APAS lietotājs nevaru labot laukus, kad statuss ir STARTED
    When Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "NEW"

    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc ID un tiek atgriezts "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"

    When Es atjaunoju melnraksta uzdevuma ipašību "<ipašības nosaukums>" "<ipašība>"
    Then Es pārliecinos ka ir atgriezts kļūdas ziņojums "Can't modify task with status <> draft"
    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<vecā īpašība>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<vecā īpašība>"

    Examples:
      | informacijas vieta               | ipašības nosaukums | ipašība                      | informacijas vieta 3         |vecā īpašība |
      | data/createTask/task/statusCode  |  title             | mans jauns title             | data/findTask/title          | TitleExample|
      | data/createTask/task/statusCode  |  description       | mans jauns apraksts          | data/findTask/description    |desct |
      | data/createTask/task/statusCode  |  typeSchema        | mana jauna tipa shema        | data/findTask/typeSchema     |null |
      | data/createTask/task/statusCode  |  typeCode          | mans jauns tipa kods         | data/findTask/typeCode       |TEST|
      | data/createTask/task/statusCode  |  prioritySchema    | mana jauna prioritates shema | data/findTask/prioritySchema |null |
      | data/createTask/task/statusCode  |  priorityCode      | HIGH                         | data/findTask/priorityCode   |LOW |

  @ICUZT16L12-102-APITests
  Scenario Outline:  Es kā APAS lietotājs nevaru labot laukus, kad statuss ir DONE
    When Es no melnraksta pāreju uz statusu Jauns "id" "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta>"
    And Atbildes satur "NEW"

    Given Es sāku šo uzdevumu "statusCode" kura vērtība "STARTED"
    When Es meklēju uzdevuma melnrakstu pēc ID un tiek atgriezts "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "STARTED"

    Then Es pabeidzu šo uzdevumu "statusCode" kura vērtība "DONE"
    When Es meklēju uzdevuma melnrakstu pēc ID un tiek atgriezts "statusCode"
    Then Es meklēju saņemtajā atbildē informāciju pēc "data/findTask/statusCode"
    And Atbildes satur "DONE"

    When Es atjaunoju melnraksta uzdevuma ipašību "<ipašības nosaukums>" "<ipašība>"
    Then Es pārliecinos ka ir atgriezts kļūdas ziņojums "Can't modify task with status <> draft"
    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<vecā īpašība>"

    When Es pieprasu informāciju par uzdevumu pēc ID "id" "<ipašības nosaukums>"
    Then Es meklēju saņemtajā atbildē informāciju pēc "<informacijas vieta 3>"
    And Atbildes satur "<vecā īpašība>"

    Examples:
      | informacijas vieta               | ipašības nosaukums | ipašība                      | informacijas vieta 3         |vecā īpašība |
      | data/createTask/task/statusCode  |  title             | mans jauns title             | data/findTask/title          | TitleExample|
      | data/createTask/task/statusCode  |  description       | mans jauns apraksts          | data/findTask/description    |desct |
      | data/createTask/task/statusCode  |  typeSchema        | mana jauna tipa shema        | data/findTask/typeSchema     |null |
      | data/createTask/task/statusCode  |  typeCode          | mans jauns tipa kods         | data/findTask/typeCode       |TEST|
      | data/createTask/task/statusCode  |  prioritySchema    | mana jauna prioritates shema | data/findTask/prioritySchema |null |
      | data/createTask/task/statusCode  |  priorityCode      | HIGH                         | data/findTask/priorityCode   |LOW |
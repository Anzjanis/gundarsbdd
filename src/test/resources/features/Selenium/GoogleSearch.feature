@Gun-26
Feature: GoogleSearch
  Kā google lietotājs, es varu meklēt informāciju google.lv pārlukā.

#  Background:  Users navigates to Company home page
#    Given I am on the "Google" page on URL "https://www.google.lv/"
#    Then I should see "Google meklēšana" message

  @UITests @SmokeTest
  Scenario Outline: Successfuls search
    Given I am on the "Google" page on URL
    Then I should see "Google meklēšana" message
    When I fill in search with "<InfoToSearch>"
    And I click on the "Search" button
    Then I am on the "rezultāti" page
    And I should see search results message
    And I should see the "<InfoToSearch>" name
    And Es kaut ko daru "param"
    Examples:
      | InfoToSearch|
      |Test |
      |yo    |
      |TestTest   |
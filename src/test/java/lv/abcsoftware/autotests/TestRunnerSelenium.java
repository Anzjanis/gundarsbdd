package lv.abcsoftware.autotests;

import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.api.testng.CucumberFeatureWrapper;
import lv.abcsoftware.autotests.jira.api.client.JiraApiClient;
import org.testng.annotations.*;
import lv.abcsoftware.autotests.jiraAPIHelper.FailedTestCases;
import lv.abcsoftware.autotests.jira.api.client.IJiraApiClient;

import java.util.ArrayList;
import java.util.List;


@CucumberOptions(
        features = "src/test/resources/features/selenium",
        glue = {"lv/abcsoftware/autotests/stepdefs/selenium"},
      //  tags = {"@UserStoryViens"},
       // strict = true,
        format = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/CucumberTestReport.json",
                "rerun:target/cucumber-reports/rerun.txt"
        })

public class TestRunnerSelenium {
    private TestNGCucumberRunner testNGCucumberRunner;
    private IJiraApiClient jiraApiClient = new JiraApiClient();

    // Apstrādā parlūku, url un jiraUpdate - Ja false, tad update netiek veikti @jira
    @Parameters({"browser", "url", "jiraUpdate"})
    @BeforeClass(alwaysRun = true)
    public void setUpClass(@Optional("Chrome") String browser, String url, @Optional("false") String jiraUpdate) throws Exception {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        System.setProperty("Browser", browser);
        System.setProperty("url", url);
        System.setProperty("jiraUpdate", jiraUpdate);
        }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }


    //Atjauno jiras statusus
    @AfterTest
    public void setJiraValues() throws UnirestException {
        if (System.getProperty("jiraUpdate").equals("true")) {
            List<String> tmpList = new ArrayList<>();
            for (String issueNumber : FailedTestCases.failed) {
                if (issueNumber.contains("@") && !(tmpList.contains(issueNumber))) {
                    tmpList.add(issueNumber);
                    jiraApiClient.setJiraLabel(issueNumber, "Failed");
                }
            }
            jiraApiClient.setJiraBody(FailedTestCases.dataMap, FailedTestCases.passed);
        }
    }

    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }
}
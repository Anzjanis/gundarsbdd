package lv.abcsoftware.autotests.stepdefs.selenium;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lv.abcsoftware.autotests.selenium.model.BaseTestSelenium;
import lv.abcsoftware.autotests.selenium.model.BrowserInfo;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import static lv.abcsoftware.autotests.stepdefs.selenium.ServiceHooks.getScenario;
import static lv.abcsoftware.autotests.selenium.util.ScreenShotUtil.takeScreeShot;


public class GoogleSearchStepdefs extends BaseTestSelenium {

    @Step
    @Then("^I should see \"([^\"]*)\" message$")
    public void iShouldSeeMessage(String arg0) throws Throwable {
       google.checkIfMessageIsVisible(arg0);
    }

    @Step
    @When("^I fill in search with \"([^\"]*)\"$")
    public void iFillInSearchWith(String arg0) throws Throwable {
      //  Assert.assertEquals(arg0, 0);
        google.fillSearchField(arg0);
    }

    @Step
    @And("^I click on the \"([^\"]*)\" button$")
    public void iClickOnTheButton(String arg0) throws Throwable {
        google.clickSearchButton();
    }

    @Step
    @Then("^I am on the \"([^\"]*)\" page$")
    public void iAmOnThePage(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        google.resultPage(arg0);
    }

    @Step
    @And("^I should see the \"([^\"]*)\" name$")
    public void iShouldSeeTheName(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        google.checkIfSearchNameIsCorrect(arg0);
    }

    @Step
    @And("^I should see search results message$")
    public void iShouldSeeSearchResultsMessage() throws Throwable {
        google.iSeeSearchResult();
        getScenario().write("VAI SIS STRADA ? :)");
        takeScreeShot(getScenario());
    }

    @Step
    @Given("^I am on the \"([^\"]*)\" page on URL \"([^\"]*)\"$")
    public void iAmOnThePageOnURL(String arg0, String arg1) throws Throwable {
        browserInfo = new BrowserInfo(System.getProperty("Browser"), arg1, "LV");
        getScenario().write("Pārlūks: " + System.getProperty("Browser") + ", URL: " + arg1);
        driver.get(arg1);
        google.checkIfBrowserInfoIsCorrect(browserInfo);
    }


    @When("^Es redzu pielikumi$")
    public void esRedzuPielikumi() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        Assert.fail("fastfail");
    }

    @And("^Es kaut ko daru$")
    public void esKautKoDaru() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^Es kaut ko daru \"([^\"]*)\"$")
    public void esKautKoDaru(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^I am on the \"([^\"]*)\" page on URL$")
    public void iAmOnThePageOnURL(String arg0) throws Throwable {
        browserInfo = new BrowserInfo(System.getProperty("Browser"), url, "LV");
        getScenario().write("Pārlūks: " + System.getProperty("Browser") + ", URL: " + url);
        driver.get(url);
        google.checkIfBrowserInfoIsCorrect(browserInfo);
    }
}

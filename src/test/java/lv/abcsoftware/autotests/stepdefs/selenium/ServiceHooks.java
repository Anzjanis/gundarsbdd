package lv.abcsoftware.autotests.stepdefs.selenium;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import lv.abcsoftware.autotests.selenium.seleniumConfiguration.SeleniumWebDriverConfiguration;
import lv.abcsoftware.autotests.jiraAPIHelper.FailedTestCases;


public class ServiceHooks {

    @Before
    public void initializeTest(){
        SeleniumWebDriverConfiguration.setDriver(System.getProperty("Browser"));
    }

    private static Scenario scenario;

    public static Scenario getScenario() {
        return scenario;
    }

    @Before
    public void before(Scenario scenario) {
        ServiceHooks.scenario = scenario;
    }

    @After
    public void embedScreenshot(Scenario scenario) {

       if (scenario.isFailed()) {
            try {
                if (System.getProperty("jiraUpdate").equals("true")) {
                    FailedTestCases.addValuesToList(scenario.getSourceTagNames().toString().split(",")[1].replace("]", "").trim(), scenario.getName()); //Saņem Tag, kurš ir savienojams ar jira issue number.
                }
                final byte[] screenshot = ((TakesScreenshot) SeleniumWebDriverConfiguration.getDriver())
                        .getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png"); //stick it in the report

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
           FailedTestCases.passed.add(scenario.getSourceTagNames().toString().split(",")[1].replace("]", "").trim());
       }

        SeleniumWebDriverConfiguration.getDriver().close();
    }
}
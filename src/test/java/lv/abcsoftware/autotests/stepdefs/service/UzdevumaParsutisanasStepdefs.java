package lv.abcsoftware.autotests.stepdefs.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;
import lv.abcsoftware.autotests.service.model.BaseTestService;
import lv.abcsoftware.autotests.service.model.taskClientModel.*;
import org.testng.Assert;

import static lv.abcsoftware.autotests.service.util.DateTime.getCurrentDate;

public class UzdevumaParsutisanasStepdefs extends BaseTestService{
private String taskID = System.getProperty("TaskID");

    private CreateDraftModel cdm = new CreateDraftModel(new AssigneeInput(), new ItemInput(), new UserInput(), new AttachmentInput());
    UzdevumuIevadeUnRedigesanaStepdefs uzdevumaParsutisanasStepdefs = new UzdevumuIevadeUnRedigesanaStepdefs();
    private static HttpResponse<JsonNode> response = null;



    @When("^Es pārsutu uzdevumu lietotājam \"([^\"]*)\" \"([^\"]*)\"$")
    public void esPārsutuUzdevumuLietotājam(String arg0, String arg1) throws Throwable {
        AssigneeInput attachmentInput = new AssigneeInput();
        attachmentInput.setDefaultAssignees(arg0);
        HttpResponse response = manipulateTask.forwardTaskWithComment(taskID, attachmentInput.getAssignee(), arg1, TaskProp.statusCode);
        uzdevumaParsutisanasStepdefs.setResponse(response); // Uzsetot response prop UzdevumaParsutisanasStepdefs
    }

    @When("^Es pieprasu informāciju par uzdevumu pēc ID \"([^\"]*)\" \"([^\"]*)\"$")
    public void esPieprasuInformācijuParUzdevumuPēcID(TaskProp arg0, TaskProp arg1) throws Throwable {
        HttpResponse response = manipulateTask.getTaskByIdWithProp(taskID, arg0, arg1);
        uzdevumaParsutisanasStepdefs.setResponse(response); // Uzsetot response prop UzdevumaParsutisanasStepdefs
    }

    @When("^Es atjaunoju melnraksta uzdevuma ipašību \"([^\"]*)\" \"([^\"]*)\"$")
    public void esAtjaunojuMelnrakstaUzdevumaIpašību(String arg0, String arg1) throws Throwable {

        String dateTime = getCurrentDate(3);
        cdm.setDeadline(dateTime);

        switch (arg0) {
            case "title": cdm.setTitle(arg1);
                break;
            case "description": cdm.setDescription(arg1);
                break;
            case "typeSchema": cdm.setTypeSchema(arg1);
                break;
            case "typeCode": cdm.setTypeCode(arg1);
                break;
            case "prioritySchema": cdm.setPrioritySchema(arg1);
                break;
            case "priorityCode": cdm.setPriorityCode(arg1);
                break;
            case "deadline": cdm.setDeadline(arg1);
                break;
            default:
                throw new IllegalArgumentException("Invalid property of TaskInput: " + arg0);
        }

        AttachmentInput attachmentInput = new AttachmentInput();
        attachmentInput.setDefaultAttachmentWithCreator();
        cdm.setAttachments(attachmentInput);

        response = manipulateTask.updateTaskWithAllPro(taskID, cdm);

    }

    @When("^Es atjaunoju uzdevuma pielikuma lauku \"([^\"]*)\" ar informāciju \"([^\"]*)\"$")
    public void esAtjaunojuUzdevumaPielikumaLaukuArInformāciju(String arg0, String arg1) throws Throwable {
        AttachmentInput attachmentInput = new AttachmentInput();
        String dateTime = getCurrentDate(3);

        String description = "";
        String fileType = "";
        String fileName = "";
        String fileRef = "";
        String fileSize = "";
        String UserInputName = "";
        String UserInputLastName = "";
        String UserInputAuthority = "";
        String UserInputId = "";

        switch (arg0) {
            case "description": description = arg1;
                break;
            case "fileType": fileType = arg1;
                break;
            case "fileName": fileName = arg1;
                break;
            case "fileRef": fileRef = arg1;
                break;
            case "fileSize": fileSize = arg1;
                break;
            case "UserInputName": UserInputName = arg1;
                break;
            case "UserInputLastName": UserInputLastName = arg1;
                break;
            case "UserInputAuthority": UserInputAuthority = arg1;
                break;
            case "UserInputId": UserInputId = arg1;
                break;
            default:
                throw new IllegalArgumentException("Invalid property of TaskInput: " + arg0);
        }
        attachmentInput.setCustomAttachmentWithCreator(description, fileType, fileName, fileSize, fileRef, UserInputName, UserInputLastName, UserInputAuthority, UserInputId);
        cdm.setAttachments(attachmentInput);
        cdm.setDeadline(dateTime);
        HttpResponse<JsonNode> respo = manipulateTask.updateTaskWithAllPro(taskID, cdm);
        uzdevumaParsutisanasStepdefs.setResponse(respo);
    }

    @Then("^Es pārliecinos ka ir atgriezts kļūdas ziņojums \"([^\"]*)\"$")
    public void esPārliecinosKaIrAtgrieztsKļūdasZiņojums(String arg0) throws Throwable {
        Assert.assertTrue(response.getBody().toString().contains(arg0), "Atbilde nesaturēja meklēto informāciju- Meklētā: " + arg0 + " iegūtā: " + response.getBody().toString());
    }
}
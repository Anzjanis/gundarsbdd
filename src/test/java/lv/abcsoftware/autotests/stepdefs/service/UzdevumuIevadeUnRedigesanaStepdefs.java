package lv.abcsoftware.autotests.stepdefs.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;
import lv.abcsoftware.autotests.service.model.BaseTestService;
import lv.abcsoftware.autotests.service.model.taskClientModel.*;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import static lv.abcsoftware.autotests.service.util.DateTime.*;


import static lv.abcsoftware.autotests.stepdefs.service.ServiceHooks.getScenario;


public class UzdevumuIevadeUnRedigesanaStepdefs extends BaseTestService {

    private CreateDraftModel cdm = new CreateDraftModel(new AssigneeInput(), new ItemInput(), new UserInput(), new AttachmentInput());
    private String searchedInfo = "va";
    private String id = null;
    private static HttpResponse<JsonNode> response = null;

    public void setResponse(HttpResponse<JsonNode> _response) {
        response = _response;
    }

    public HttpResponse<JsonNode> getResponse() {
        return response;
    }


    private String attachmentID = "";

    @Step
    @When("^Es izpildu createDraft metodi$")
    public void esIzpilduCreateDraftMetodi() {
        String dateTime = getCurrentDate(3);
        cdm.setDeadline(dateTime);
        response = createTask.createDraftWithID(TaskProp.id, TaskProp.statusCode, cdm);
    }

    @Step
    @And("^Atbildes statuss ir \"([^\"]*)\"$")
    public void atbildesStatussIr(int arg0) {
        Assert.assertEquals(arg0, response.getStatus(), "Atbildes statuss nav korekts");
    }

    @Step
    @When("^Es meklēju uzdevuma melnrakstu pēc \"([^\"]*)\" \"([^\"]*)\"$")
    public void esMeklējuUzdevumaMelnrakstuPēc(String arg0, TaskProp arg1) throws Throwable {
        response = manipulateTask.getTaskByIdWithProp(arg0 + id, arg1);
    }

    @Step
    @Given("^Es atjaunoju melnraksta title uzdevumu \"([^\"]*)\"$")
    public void esAtjaunojuMelnrakstaTitleUzdevumu(String arg0){

      cdm.setTitle(arg0);
      manipulateTask.updateTask(id, cdm.getCreator() + cdm.getTitle());
    }

    @Step
    @Then("^Es meklēju saņemtajā atbildē informāciju pēc \"([^\"]*)\"$")
    public void esMeklējuSaņemtajāAtbildēInformācijuPēc(String arg0) throws Throwable {
        searchedInfo = jsonParser.getJsonNode(response.getBody().toString(), arg0).textValue();
        getScenario().write("Sanemta atbilde" + arg0 + ": " + searchedInfo);
    }

    @Step
    @And("^Atbildes satur \"([^\"]*)\"$")
    public void atbildesSatur(String arg0) {
       Assert.assertEquals(searchedInfo + "", arg0, "Vērtības nesakrita");
    }

    @Step
    @Then("^Es meklēju saņemtajā atbildē informāciju par ID \"([^\"]*)\"$")
    public void esMeklējuSaņemtajāAtbildēInformācijuParID(String arg0) throws Throwable {
        id = jsonParser.getJsonNode(response.getBody().toString(), arg0).textValue();
        System.setProperty("TaskID", id);
        getScenario().write("Melnraksta" + arg0 + ": " + id);
    }

    @Step
    @Given("^Es atjaunoju melnraksta aprakstu \"([^\"]*)\"$")
    public void esAtjaunojuMelnrakstaAprakstu(String arg0) throws Throwable {
        cdm.setDescription(arg0);
        manipulateTask.updateTask(id, cdm.getCreator() + cdm.getDescription());
    }

    @Step
    @Given("^Es izdzēšu uzdevuma melnrakstu$")
    public void esIzdzēšuUzdevumaMelnrakstu() throws Throwable {
      Assert.assertTrue(jsonParser.getJsonNode(manipulateTask.deleteDraft(id).getBody().toString(), "data/deleteTaskDraft").asBoolean(), "Uzdevums netika izdzēsts: " + id);
    }


    @Step
    @Given("^Es melnrakstam pievienoju pielikumu \"([^\"]*)\" \"([^\"]*)\"$")
    public void esMelnrakstamPievienojuPielikumu(String arg0, TaskProp arg1) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        AttachmentInput attachmentInput = new AttachmentInput();
        attachmentInput.setDefaultAttachment(arg0);
        response = manipulateTask.addAttachment(id, attachmentInput.getAttachment() + cdm.getCreator(), TaskProp.createDate,arg1);

        Assert.assertTrue(response.getBody().toString().contains(dateFormat.format(date)), "Uzdevums nesatur pareizu datumu: " + response.getBody().toString());
    }


    @And("^Atbilde satures \"([^\"]*)\" un informacija atrodas \"([^\"]*)\"$")
    public void atbildeSaturesUnInformacijaAtrodas(String arg0, String arg1) throws Throwable {
        searchedInfo = jsonParser.getJsonNode(response.getBody().toString(), arg1).toString().replace("\"", "");
        Assert.assertEquals(searchedInfo + "", arg0, "Vērtības nesakrita");
    }

	@Then("^Es meklēju saņemtajā atbildē informācijas masīvu pēc \"([^\"]*)\"$")
	public void esMeklējuSaņemtajāAtbildēInformācijasMasīvuPēc(String arg0) throws Throwable {
		searchedInfo = jsonParser.getJsonNode(response.getBody().toString(), arg0).toString().replace("\"", "");
		getScenario().write("Sanemta atbilde" + arg0 + ": " + searchedInfo);
	}

    @Then("^Es pabeidzu šo uzdevumu \"([^\"]*)\" kura vērtība \"([^\"]*)\"$")
    public void esPabeidzuŠoUzdevumuKuršAtrodas(TaskProp arg0, String arg1) {
        // Write code here that turns the phrase above into concrete actions
        response = manipulateTask.finishTask(id, arg0);
        Assert.assertTrue(response.getBody().toString().contains(arg1), "Atbilde nestatur vajadzīgo statusu: " + arg1);
    }

    @Then("^Es sāku šo uzdevumu \"([^\"]*)\" kura vērtība \"([^\"]*)\"$")
    public void esSākuŠoUzdevumuKuraVērtība(TaskProp arg0, String arg1) {
        response = manipulateTask.startTask(id, arg0);
        Assert.assertTrue(response.getBody().toString().contains(arg1), "Atbilde nestatur vajadzīgo statusu: " + arg1 +"  " +response.getBody());
    }

    @Given("^Es atgriežu uzdevumu \"([^\"]*)\" kura vērtība \"([^\"]*)\"$")
    public void esAtgriežuUzdevumuKuraVērtība(TaskProp arg0, String arg1) {
        response = manipulateTask.returnTask(id, arg0);
        Assert.assertTrue(response.getBody().toString().contains(arg1), "Atbilde nestatur vajadzīgo statusu: " + arg1);
    }


    @Given("^Es pārsūtu uzdevumu \"([^\"]*)\" kura vērtība \"([^\"]*)\"$")
    public void esPārsūtuUzdevumuKuraVērtība(TaskProp arg0, String arg1) {

        AssigneeInput attachmentInput = new AssigneeInput();
        attachmentInput.setDefaultAssignees(arg1);
       // cdm.setAttachments();
        response = manipulateTask.forwardTask(id, attachmentInput.getAssignee(), arg0);
        Assert.assertTrue(response.getBody().toString().contains(arg1), "Atbilde nestatur vajadzīgo statusu: " + arg1 +"  " +response.getBody());
    }

    @Given("^Es izdzēšu uzdevumu pielikumu \"([^\"]*)\" kura vērtība \"([^\"]*)\"$")
    public void esIzdzēšuUzdevumuPielikumuKuraVērtība(String arg0, String arg1) {
        response = manipulateTask.deleteAttachment(id,attachmentID);
        Assert.assertTrue(response.getBody().toString().contains(arg1), "Atbilde nestatur vajadzīgo statusu: " + arg1 + " " + response.getBody().toString() );
    }

    @Given("^Es melnrakstam pievienoju pielikumu \"([^\"]*)\" \"([^\"]*)\" informācijas vieta \"([^\"]*)\"$")
    public void esMelnrakstamPievienojuPielikumuInformācijasVieta(String arg0, TaskProp arg1, String arg2) throws Throwable {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        AttachmentInput attachmentInput = new AttachmentInput();
        attachmentInput.setDefaultAttachment(arg0);
        response = manipulateTask.addAttachment(id, attachmentInput.getAttachment() + cdm.getCreator(), TaskProp.createDate, arg1);

        attachmentID = jsonParser.getJsonNode(response.getBody().toString(), arg2).textValue();
        Assert.assertTrue(response.getBody().toString().contains(dateFormat.format(date)), "Uzdevums nesatur pareizu datumu: " + response.getBody().toString());
    }

    @Given("^Es no melnraksta pāreju uz statusu Jauns \"([^\"]*)\" \"([^\"]*)\"$")
    public void esNoMelnrakstaPārejuUzStatusuJauns(TaskProp arg0, TaskProp arg1) {
        response = manipulateTask.CreateTaskFromDraft(id, arg0,arg1);
        Assert.assertTrue(response.getBody().toString().contains("NEW"), "Atbilde nesatur statusu NEW: " + response.getBody().toString() );
    }
//

    @Then("^Es atjaunoju uzdevuma pielikumu \"([^\"]*)\" ar informāciju \"([^\"]*)\"$")
    public void esAtjaunojuUzdevumaPielikumu(TaskProp arg0, String arg1) {

        AttachmentInput attachmentInput = new AttachmentInput();
        attachmentInput.setDefaultAttachment(arg1);

        response = manipulateTask.updateAttachment(id,attachmentID, attachmentInput.getAttachment() + cdm.getCreator(), arg0);
    }

	@Given("^Es uzdevumam pievienoju komentāru \"([^\"]*)\"$")
	public void esUzdevumamPievienojuKomentāru(String arg0) {
        cdm.setComment(arg0);
		response = manipulateTask.createComment(id, cdm.getComment(), TaskProp.comment);
	}

    @And("^Atbildes satur šodienas datumu$")
    public void atbildesSaturŠodienasDatumu() throws Throwable {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        Assert.assertTrue(searchedInfo.contains(dateFormat.format(date)), "Uzdevums nesatur šodineas datumu: " + searchedInfo);
    }

    @When("^Es meklēju uzdevuma melnrakstu pēc ID un tiek atgriezts \"([^\"]*)\"$")
    public void esMeklējuUzdevumaMelnrakstuPēcIDUnTiekAtgriezts(TaskProp arg0) {
        response = manipulateTask.getTaskByIdWithProp(id, arg0);
    }

    @Then("^Es melnrakstam pievienoju pielikumu \"([^\"]*)\"$")
    public void esMelnrakstamPievienojuPielikumu(String arg0) throws Throwable {

        AttachmentInput attachmentInput = new AttachmentInput();
        attachmentInput.setDefaultAttachment(arg0);
        response = manipulateTask.addAttachment(id, attachmentInput.getAttachment() + cdm.getCreator(), TaskProp.createDate, TaskProp.statusCode);

    }
}
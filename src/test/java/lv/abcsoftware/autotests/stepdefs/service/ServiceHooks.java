package lv.abcsoftware.autotests.stepdefs.service;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import lv.abcsoftware.autotests.jiraAPIHelper.FailedTestCases;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import lv.abcsoftware.autotests.selenium.seleniumConfiguration.SeleniumWebDriverConfiguration;


public class ServiceHooks {

    @Before
    public void initializeTest(){

    }

    private static Scenario scenario;

    public static Scenario getScenario() {
        return scenario;
    }

    @Before
    public void before(Scenario scenario) {
        ServiceHooks.scenario = scenario;
    }

    @After
    public void embedScreenshot(Scenario scenario) {

       if (scenario.isFailed()) {
            try {
                if (System.getProperty("jiraUpdate").equals("true")) {
                    FailedTestCases.addValuesToList(scenario.getSourceTagNames().toString().split(",")[1].replace("]", "").trim(), scenario.getName()); //Saņem Tag, kurš ir savienojams ar jira issue number.
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
           FailedTestCases.passed.add(scenario.getSourceTagNames().toString().split(",")[1].replace("]", "").trim());
       }
    }
}
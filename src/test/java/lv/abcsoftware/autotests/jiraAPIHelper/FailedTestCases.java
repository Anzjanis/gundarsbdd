package lv.abcsoftware.autotests.jiraAPIHelper;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FailedTestCases {

    public static List<String> failed = new ArrayList<>();
    public static Map<String, List<String>> dataMap = new HashMap<>();
    public static List<String> passed = new ArrayList<>();


    public static void addValuesToList(String key, String value)
    {
        failed.add(key);

        if (!dataMap.containsKey(key)) {
            dataMap.put(key, new ArrayList<>());
        }
        dataMap.get(key).add(value);
    }
}

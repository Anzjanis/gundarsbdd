package lv.abcsoftware.autotests;

import com.mashape.unirest.http.exceptions.UnirestException;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import lv.abcsoftware.autotests.jira.api.client.IJiraApiClient;
import lv.abcsoftware.autotests.jira.api.client.JiraApiClient;
import lv.abcsoftware.autotests.jiraAPIHelper.FailedTestCases;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;


@CucumberOptions(
        features = "src/test/resources/features/service",
        glue = {"lv/abcsoftware/autotests/stepdefs/service"},
      //  tags = {"@UserStoryViens"},
       // strict = true,
        format = {
                "pretty",
                "html:target/cucumber-reports/cucumber-pretty",
                "json:target/cucumber-reports/CucumberTestReport.json",
                "rerun:target/cucumber-reports/rerun.txt"
        })

public class TestRunnerService {
    private TestNGCucumberRunner testNGCucumberRunner;
    private IJiraApiClient jiraApiClient = new JiraApiClient();

    // Apstrādā parlūku, url un jiraUpdate - Ja false, tad update netiek veikti @jira
    @Parameters({"jiraUpdate"})
    @BeforeClass(alwaysRun = true)
    public void setUpClass(@Optional("false") String jiraUpdate) {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        System.setProperty("jiraUpdate", jiraUpdate);
        }

    @AfterClass(alwaysRun = true)
    public void tearDownClass() throws Exception {
        testNGCucumberRunner.finish();
    }

    @Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
    public void feature(CucumberFeatureWrapper cucumberFeature) {
        testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
    }

    //Atjauno jiras statusus
    @AfterTest
    private void setJiraValues() throws UnirestException {
        if (System.getProperty("jiraUpdate").equals("true")) {
            List<String> tmpList = new ArrayList<>();
            for (String issueNumber : FailedTestCases.failed) {
                if (issueNumber.contains("@") && !(tmpList.contains(issueNumber))) {
                    tmpList.add(issueNumber);
                    jiraApiClient.setJiraLabel(issueNumber, "Failed");
                }
            }
            jiraApiClient.setJiraBody(FailedTestCases.dataMap, FailedTestCases.passed);
        }
    }


    @DataProvider
    public Object[][] features() {
        return testNGCucumberRunner.provideFeatures();
    }
}
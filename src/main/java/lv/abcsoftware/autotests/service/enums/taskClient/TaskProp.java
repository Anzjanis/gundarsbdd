package lv.abcsoftware.autotests.service.enums.taskClient;

public enum TaskProp {
    number("number"),
    typeSchema("typeSchema"),
    title("title"),
    description("description"),
    typeCode("typeCode"),
    statusSchema("statusSchema"),
    statusCode("statusCode"),
    prioritySchema("prioritySchema"),
    priorityCode("priorityCode"),
    createDate("createDate"),
    deadline("deadline"),
    creator("creator {id name lastname authority}"),
    attachments("attachments {description}"),
    attachmentsFull("attachments {id fileName fileType createDate fileSize description fileRef creator {id name lastname authority}}"),
    history("history { assignee {  authority        user {   id  }      }      statusCode      changeTypeCode    }   "),
    comments("comments{ description creator{ name lastname authority }}"),
    comment("comment{ description createDate creator{ name lastname authority }}"),
    items("items {typeSchema typeCode number }"),
    id("id"),
    finishDate("finishDate"),
    assignees("assignees");

    private String text;
    TaskProp(String text)
    {
        this.text = text;
    }
    @Override
    public String toString() {
        return text;
    }
}

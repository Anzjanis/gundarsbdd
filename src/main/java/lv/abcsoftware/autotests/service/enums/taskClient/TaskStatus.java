package lv.abcsoftware.autotests.service.enums.taskClient;

public enum TaskStatus {
    DRAFT("DRAFT"),
    NEW ("NEW"),
    STARTED("STARTED"),
    DONE("DONE");

    private String text;
    TaskStatus(String text)
    {
        this.text = text;
    }
    @Override
    public String toString() {
        return text;
    }
}

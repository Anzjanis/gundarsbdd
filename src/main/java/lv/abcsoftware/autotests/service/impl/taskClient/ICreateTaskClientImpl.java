package lv.abcsoftware.autotests.service.impl.taskClient;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;
import lv.abcsoftware.autotests.service.model.taskClientModel.CreateDraftModel;

public interface ICreateTaskClientImpl {

   HttpResponse<JsonNode> createDraftWithID(TaskProp taskProp, TaskProp taskProp2, CreateDraftModel dm);
}

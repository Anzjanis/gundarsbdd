package lv.abcsoftware.autotests.service.impl.taskClient;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;
import lv.abcsoftware.autotests.service.graphiQL.client.taskClient.ManipulateTaskContent;
import lv.abcsoftware.autotests.service.model.taskClientModel.AttachmentInput;
import lv.abcsoftware.autotests.service.model.taskClientModel.CreateDraftModel;
import lv.abcsoftware.autotests.service.model.taskClientModel.UserInput;

public class ManipulateTaskClientImpl implements IManipulateTaskClientImpl {

    private ManipulateTaskContent mp = new ManipulateTaskContent();
    @Override
    public HttpResponse<JsonNode> getTaskByIdWithProp(String id, TaskProp a) {
        return mp.getTaskInfoById(id, a);
    }

    @Override
    public HttpResponse<JsonNode> getTaskByIdWithProp(String id, TaskProp a, TaskProp b) {
        return mp.getTaskInfoById(id, a, b);
    }

    @Override
    public HttpResponse<JsonNode> updateTask(String id, String infoToUpdarte) {

        return mp.updateTask(id, infoToUpdarte);
    }
    @Override
    public HttpResponse<JsonNode> updateTaskWithAllPro(String id, CreateDraftModel cdm) {

        return mp.updateTask(id, cdm.getCreator() +
                cdm.getDescription() +
                cdm.getTypeSchema() +
                cdm.getTypeCode() +
                cdm.getTitle() +
                cdm.getPrioritySchema() +
                cdm.getPriorityCode() +
                cdm.getDeadline() +
                cdm.getAttachments() +
                cdm.getItems() +
                cdm.getAssignees());
    }

    @Override
    public HttpResponse<JsonNode> createComment(String id, String comment, TaskProp prop) {
        return mp.createComment(id, comment, prop);
    }

    @Override
    public HttpResponse<JsonNode> deleteDraft(String id) {
        return mp.deleteDraft(id);
    }

    @Override
    public HttpResponse<JsonNode> addAttachment(String id, String attachmentInput, TaskProp arg0, TaskProp arg1) {
        return mp.addAttachment(id, attachmentInput, arg0,arg1);
    }

    @Override
    public HttpResponse<JsonNode> CreateTaskFromDraft(String id, TaskProp arg0, TaskProp arg1) {
        return mp.CreateTaskFromDraft(id, arg0, arg1);
    }

    @Override
    public HttpResponse<JsonNode> finishTask(String id, TaskProp arg0) {
        return mp.finishTask(id, arg0);
    }

    @Override
    public HttpResponse<JsonNode> startTask(String id, TaskProp arg0) {
        return mp.startTask(id, arg0);
    }

    @Override
    public HttpResponse<JsonNode> returnTask(String id, TaskProp arg0) {
        return mp.returnTask(id, arg0);
    }

    @Override
    public HttpResponse<JsonNode> forwardTask(String id, String assignee, TaskProp arg0) {
        return mp.forwardTask(id,assignee, arg0);
    }

    @Override
    public HttpResponse<JsonNode> forwardTaskWithComment(String id, String assignee, String comment, TaskProp arg0) {
        return mp.forwardTaskWithComment(id, assignee, comment, arg0);}

    @Override
    public HttpResponse<JsonNode> deleteAttachment(String id, String attachmentID) {
        return mp.deleteAttachment(id, attachmentID);
    }

    @Override
    public HttpResponse<JsonNode> updateAttachment(String id,String attachmentID, String attachmentInfo,  TaskProp arg0) {
        return mp.updateAttachment(id,attachmentID, attachmentInfo, arg0);
    }
}

package lv.abcsoftware.autotests.service.impl.taskClient;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;
import lv.abcsoftware.autotests.service.model.taskClientModel.AttachmentInput;
import lv.abcsoftware.autotests.service.model.taskClientModel.CreateDraftModel;

public interface IManipulateTaskClientImpl {

    HttpResponse<JsonNode> getTaskByIdWithProp(String id, TaskProp a);

    HttpResponse<JsonNode> getTaskByIdWithProp(String id, TaskProp a, TaskProp b);

    HttpResponse<JsonNode> deleteDraft(String id);

    HttpResponse<JsonNode> addAttachment(String id, String attachmentInput, TaskProp arg0, TaskProp arg1);

    HttpResponse<JsonNode> CreateTaskFromDraft(String id, TaskProp arg0, TaskProp arg1);

    HttpResponse<JsonNode> finishTask(String id, TaskProp arg0);

    HttpResponse<JsonNode> startTask(String id, TaskProp arg0);

    HttpResponse<JsonNode> returnTask(String id, TaskProp arg0);

    HttpResponse<JsonNode> forwardTask(String id,String assignee,  TaskProp arg0);

    HttpResponse<JsonNode> forwardTaskWithComment(String id, String assignee, String comment, TaskProp arg0);

    HttpResponse<JsonNode> deleteAttachment(String id, String attachmentID);

    HttpResponse<JsonNode> updateAttachment(String id, String attachmentID, String attachmentUpdateInfo, TaskProp arg0);

    HttpResponse<JsonNode> updateTask(String id, String infoToUpdarte);

    HttpResponse<JsonNode> updateTaskWithAllPro(String id, CreateDraftModel cdm);

	HttpResponse<JsonNode> createComment(String id, String comment,TaskProp prop);
}

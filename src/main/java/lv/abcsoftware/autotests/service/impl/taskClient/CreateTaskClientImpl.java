package lv.abcsoftware.autotests.service.impl.taskClient;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;
import lv.abcsoftware.autotests.service.graphiQL.client.taskClient.CreateTask;
import lv.abcsoftware.autotests.service.graphiQL.client.taskClient.ManipulateTaskContent;
import lv.abcsoftware.autotests.service.model.taskClientModel.CreateDraftModel;

public class CreateTaskClientImpl implements ICreateTaskClientImpl {

    @Override
    public HttpResponse<JsonNode> createDraftWithID(TaskProp taskProp,TaskProp taskProp2, CreateDraftModel dm)
    {
        CreateTask ct = new CreateTask();

        return ct.createTaskDraft(taskProp, taskProp2, dm.getDeadline(), dm.getAssignees(), dm.getTypeCode(), dm.getDescription(), dm.getTitle(), dm.getCreator(), dm.getPriorityCode(), dm.getAttachments(), dm.getCreator());
    }
}

package lv.abcsoftware.autotests.service.model.taskClientModel;

public class UserInput {
    private String userInp = "{id: \\\"test1\\\", name: \\\"Jānis\\\", lastname: \\\"Bērziņš\\\", authority: \\\"Iecirknis\\\"},";

    public String getUserInp() {
        return userInp;
    }

    public void setUserInp(String id, String name, String lastName, String authority) {

        this.userInp = String.format("[{authority: \\\"%s\\\", user: {name: \\\"%s\\\", %s: \\\"%s\\\", id: \\\"%s\\\"}}]", authority, name, lastName, id);
    }
}

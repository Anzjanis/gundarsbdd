package lv.abcsoftware.autotests.service.model;

import lv.abcsoftware.autotests.service.impl.taskClient.CreateTaskClientImpl;
import lv.abcsoftware.autotests.service.impl.taskClient.ICreateTaskClientImpl;
import lv.abcsoftware.autotests.service.impl.taskClient.IManipulateTaskClientImpl;
import lv.abcsoftware.autotests.service.impl.taskClient.ManipulateTaskClientImpl;
import lv.abcsoftware.autotests.service.util.JsonParser;

public class BaseTestService {
   protected ICreateTaskClientImpl createTask = new CreateTaskClientImpl();
   protected IManipulateTaskClientImpl manipulateTask = new ManipulateTaskClientImpl();
   protected JsonParser jsonParser = new JsonParser();
}

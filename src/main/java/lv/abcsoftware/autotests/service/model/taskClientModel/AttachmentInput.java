package lv.abcsoftware.autotests.service.model.taskClientModel;

public class AttachmentInput {

    private String attachment = "attachments: [{description: \\\"test\\\", fileType: \\\"exe\\\", fileName: \\\"test\\\", fileRef: \\\"123\\\", fileSize: 100,";

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String description, String fileType, String fileName, String fileRef, int fileSize) {
        this.attachment = String.format("attachment: [{description: \\\"%s\\\", fileType: \\\"%s\\\", fileName: \\\"%s\\\", fileRef: \\\"%s\\\", fileSize: %d,", description, fileType, fileName, fileRef, fileSize);
    }

    public void setAttachment(String description) {
        this.attachment = String.format("attachment: [{description: \\\"%s\\\", fileType: \\\"exe\\\", fileName: \\\"test\\\", fileRef: \\\"123\\\", fileSize: 100,", description);
    }

    //Ja vajag pielikt jaunu attachmentu
    public void setDefaultAttachment(String description) {
        this.attachment = String.format("attachment: {description: \\\"%s\\\", fileType: \\\"exe\\\", fileName: \\\"test\\\", fileRef: \\\"123\\\", fileSize: 100,", description);
    }

    public void setDefaultAttachmentWithCreator()
    {
        this.attachment = String.format("attachments: [{description: \\\"some description\\\", fileType: \\\"exe\\\", fileName: \\\"test\\\", fileRef: \\\"123\\\", fileSize: 100, creator: { name: \\\" test\\\", lastname: \\\"userInputLast\\\", authority: \\\"userInputAuthority\\\", id:\\\"userInputID\\\"}}]");
    }

    public void  setCustomAttachmentWithCreator(String description, String fileType, String fileName, String fileSize, String fileRef,  String UserInputName, String UserInputLastName, String UserInputAuthority, String UserInputId)
    {
        String _description = (description.isEmpty()) ? "default description" : description;
        String _fileType = (fileType.isEmpty()) ? "exe" : fileType;
        String _fileName = (fileName.isEmpty()) ? "default fileName" : fileName;
        String _fileSize = (fileSize.isEmpty()) ? "100" : fileSize;
        String _fileRef = (fileRef.isEmpty()) ? "URN:IVIS:200266:DOC-1000002605-V0.1" : fileRef;
        String _UserInputName = (UserInputName.isEmpty()) ? "default UserInputName" : UserInputName;
        String _UserInputLastName = (UserInputLastName.isEmpty()) ? "default UserInputLastName" : UserInputLastName;
        String _UserInputAuthority = (UserInputAuthority.isEmpty()) ? "default UserInputAuthority" : UserInputAuthority;
        String _UserInputId = (UserInputId.isEmpty()) ? "default UserInputId" : UserInputId;


        this.attachment = String.format("attachments: [{" +
                "description: \\\"" + _description + "\\\"," +
                "fileType: \\\"" + _fileType + "\\\", " +
                "fileName: \\\"" + _fileName + "\\\", " +
                "fileRef: \\\"" + _fileRef + "\\\", " +
                "fileSize: " + _fileSize + ", " +
                "creator: { name: \\\"" + _UserInputName + "\\\", " +
                "lastname: \\\"" + _UserInputLastName + "\\\", " +
                "authority: \\\"" + _UserInputAuthority + "\\\", " +
                "id: \\\"" + _UserInputId + "\\\" " +
                "}}],");
    }
}

package lv.abcsoftware.autotests.service.model.taskClientModel;

public class ItemInput {
    private String typeSchema = "typeSchema: \\\"Example typeSchema\\\",";
    private String typeCode = "typeCode: \\\"TEST\\\",";
    private String number = "number: \\\"mynumber\\\",";

    public String getTypeSchema() {
        return typeSchema;
    }

    public void setTypeSchema(String typeSchema) {
        this.typeSchema = typeSchema;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

package lv.abcsoftware.autotests.service.model.taskClientModel;

public class AssigneeInput {
    private String assignee = "[{authority: \\\"iecirknis\\\", user: {name: \\\"jānis\\\", lastname: \\\"bērziņs\\\", id: \\\"123\\\"}}]";

    public String getAssignee() {
        return assignee;
    }

    public void setAssignees(String authority, String name, String lastName, String id) {

        this.assignee = String.format("[{authority: \\\"%s\\\", user: {name: \\\"%s\\\", %s: \\\"%s\\\", id: \\\"%s\\\"}}]", authority, name, lastName, id);
    }

    public void setDefaultAssignees(String authority) {

        this.assignee = String.format("{authority: \\\"iecirknis\\\", user: {name: \\\"jānis\\\", lastname: \\\"bērziņs\\\", id: \\\"123\\\"}}", authority);
    }
}

package lv.abcsoftware.autotests.service.model.taskClientModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CreateDraftModel {
    public CreateDraftModel(AssigneeInput assigneeInput, ItemInput itemInput, UserInput userInput, AttachmentInput attachmentInput)
    {
        setAssignees(assigneeInput);
        setItems(itemInput);
        setCreator(userInput);
        setAttachments(attachmentInput);
    }

    public CreateDraftModel()
    {

    }

    private String number = "number: \\\"numberExample\\\", ";

    private String title = "title: \\\"TitleExample\\\", ";

    private String description = "description: \\\"desct\\\",";

    private String typeSchema = "typeSchema: \\\"Example typeSchema\\\",";

    private String typeCode = "typeCode: \\\"TEST\\\",";

    private String prioritySchema = "prioritySchema: \\\"Example prioritySchema\\\",";

    private String priorityCode = "priorityCode: \\\"LOW\\\",";

    private String deadline = "deadline: \\\"2018-04-09T00:00\\\",";

   // private String deadline = "deadline: \\\"" + getFutureDateTime() + " \\\"";

    private String comment = "comment: {description: \\\"comment3\\\", creator: {name: \\\"test\\\", lastname: \\\"test\\\", authority: \\\"test\\\", id: \\\"123\\\"";

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = String.format("comment: {description: \\\"%s\\\", creator: {name: \\\"test\\\", lastname: \\\"test\\\", authority: \\\"test\\\", id: \\\"123\\\"", comment);

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        this.title = String.format("title: \\\"%s\\\", ", title);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = String.format("description: \\\"%s\\\", ", description);
    }

    public String getTypeSchema() {
        return typeSchema;
    }

    public void setTypeSchema(String typeSchema) { this.typeSchema = String.format("typeSchema: \\\"%s\\\", ", typeSchema); }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = String.format("typeCode: \\\"%s\\\", ", typeCode);
    }

    public String getPrioritySchema() {
        return prioritySchema;
    }

    public void setPrioritySchema(String prioritySchema) { this.prioritySchema = String.format("prioritySchema: \\\"%s\\\", ", prioritySchema); }

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) { this.priorityCode = String.format("priorityCode: \\\"%s\\\", ", priorityCode);
    }


    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = String.format("deadline: \\\"%s\\\",", deadline);
    }

    //UserInput
    public String getCreator() {

        return creator;
    }

    public void setCreator(UserInput userInput) {
        String a;
        a = "creator: " + userInput.getUserInp();
        this.creator = a;
    }

    private String creator;
    private String attachments;
    private String items;
    private String assignees;

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(AttachmentInput attachments) {
        String a;
         a = attachments.getAttachment();

        this.attachments = a;
    }

    public String getItems() {
        return items;
    }

    public void setItems(ItemInput items) {
        String a;
        a = "items: [{ " + items.getTypeSchema()+ " " + items.getNumber() + " " + items.getTypeCode() + "}],";
        this.items = a;
    }

    public String getAssignees() {
        return assignees;
    }

    public void setAssignees(AssigneeInput assignees) {
        String a;

        a = "assignees: " + assignees.getAssignee();

        this.assignees = a;
    }

    public String getNumber() {
        return number;
    }
}

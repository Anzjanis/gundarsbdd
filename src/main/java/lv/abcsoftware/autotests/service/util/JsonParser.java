package lv.abcsoftware.autotests.service.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;

public class JsonParser {

    private ObjectMapper mapper = new ObjectMapper();
    private JsonNode jsonNode = null;


    // Padots pilnais ceļš uz meklējamo objektu, piemēram,  data/createTaskDraft/task/id -> {"data":{"createTaskDraft":{"task":{"id":"6e59f7bd-f598-4bb5-9eae-84d63fb3fa9a"}}}}
    public JsonNode getJsonNode(String jsonToParse, String searchNode) throws InvalidPropertiesFormatException {
        String[] jsonNodeArray;
        try {
            jsonNodeArray = searchNode.split("/");
            jsonNode = mapper.readTree(jsonToParse);
            for (String aJsonNodeArray : jsonNodeArray) {

                if (jsonNode.isArray()) {
                    for (JsonNode node : jsonNode) {
                        jsonNode = node;
                    }
                }

                jsonNode = jsonNode.get(aJsonNodeArray);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
            throw new InvalidPropertiesFormatException("Meklējāmā node nav noformēta pareizi: " + searchNode);
        }
        return jsonNode;
    }


    public JsonNode getJsonNode(String jsonToParse) {
        try {
            jsonNode = mapper.readTree(jsonToParse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonNode;
    }
}
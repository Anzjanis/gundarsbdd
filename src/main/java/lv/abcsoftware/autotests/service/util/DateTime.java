package lv.abcsoftware.autotests.service.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DateTime {
    public static String getCurrentDate(int day)
    {
        String dateTime;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, + day);
        dateTime = format.format(calendar.getTime());

        return dateTime;
    }
}

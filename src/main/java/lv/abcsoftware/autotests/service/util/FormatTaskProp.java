package lv.abcsoftware.autotests.service.util;

import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;

public class FormatTaskProp {
    public static String formatTaskPropToString(TaskProp[] properties)
    {

        StringBuilder propList = new StringBuilder(" ");
        for (TaskProp prop : properties) {
            propList.append(prop).append(" ");
        }

        return propList.toString();
    }
}

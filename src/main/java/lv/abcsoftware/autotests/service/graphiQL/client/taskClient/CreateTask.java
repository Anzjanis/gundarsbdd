package lv.abcsoftware.autotests.service.graphiQL.client.taskClient;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;

public class CreateTask{

     //private String url = "http://192.168.101.66:30000/graphql";

    private String url = "http://k8s-node1:31214/elieta-taskmanagement-api/graphql";

    public HttpResponse<JsonNode> createTaskDraft(TaskProp whichProp, TaskProp whichProp2, String... properties)
    {
        String propValue = whichProp.toString() + " " + whichProp2.toString();
        StringBuilder propList = new StringBuilder(" ");

        for(String prop : properties)
        {
            propList.append(prop).append(" ");
        }
        String query = String.format("{ \"query\": \" mutation createDraft {  createTaskDraft(input: {task: {%s}]}}) {    task {      %s    }  }}\" }", propList.toString(), propValue);

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return jsonNodeHttpResponse;
    }
}

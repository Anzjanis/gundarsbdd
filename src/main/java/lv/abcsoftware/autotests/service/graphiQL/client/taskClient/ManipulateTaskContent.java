package lv.abcsoftware.autotests.service.graphiQL.client.taskClient;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lv.abcsoftware.autotests.service.enums.taskClient.TaskProp;
import lv.abcsoftware.autotests.service.model.taskClientModel.AttachmentInput;
import lv.abcsoftware.autotests.service.model.taskClientModel.UserInput;
import static lv.abcsoftware.autotests.service.util.FormatTaskProp.formatTaskPropToString;

public class ManipulateTaskContent{

    private String url = "http://k8s-node1:31214/elieta-taskmanagement-api/graphql";
   // private String url = "http://192.168.101.66:30000/graphql";

    public HttpResponse<JsonNode> getTaskInfoById(String id, TaskProp... properties)
    {
        String query = String.format("{ \"query\": \"{ findTask(id: \\\"%s\\\") { %s } }\" }", id, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode>  updateTask(String id, String infoToUpdate) {
       String query = String.format("{ \"query\": \" mutation updateTaskDraft {  updateTaskDraft(input: {taskId: \\\"%s\\\", task: {%s}}) {    task {      %s } }  }\" }",id, infoToUpdate, TaskProp.number);

            HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> deleteDraft(String id) {

        String query = String.format("{ \"query\": \" mutation deleteTask {  deleteTaskDraft(input: {taskId: \\\"%s\\\"}) }\" }",id);

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return jsonNodeHttpResponse;

    }

    public HttpResponse<JsonNode> addAttachment(String id, String attachmentInput, TaskProp... properties) {

        String query = String.format("{ \"query\": \" mutation createAttachment {  createAttachment(input: {taskId: \\\"%s\\\", %s}}) {    attachment {      %s } }  }\" }",id, attachmentInput, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> CreateTaskFromDraft(String id, TaskProp... properties) {

        String query = String.format("{ \"query\": \" mutation createTask {  createTask(input: {taskId: \\\"%s\\\"}) {    task{      %s } }  }\" }",id, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> finishTask(String id,  TaskProp... properties) {

        String query = String.format("{ \"query\": \" mutation finishTask {  finishTask(input: {taskId: \\\"%s\\\"}) {    task{      %s } }  }\" }",id, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;

    }

    public HttpResponse<JsonNode> startTask(String id, TaskProp... properties) {

        String query = String.format("{ \"query\": \" mutation startTask {  startTask(input: {taskId: \\\"%s\\\"}) {    task{      %s } }  }\" }",id, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;

    }

    public HttpResponse<JsonNode> returnTask(String id,  TaskProp... properties) {

        String query = String.format("{ \"query\": \" mutation returnTask {  returnTask(input: {taskId: \\\"%s\\\"}) {    task{      %s } }  }\" }", id, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> forwardTask(String id,String assignee, TaskProp... properties) {
        String query = String.format("{ \"query\": \" mutation forwardTask {  forwardTask(input: {taskId: \\\"%s\\\", assignee: %s}) {    task{      %s } }  }\" }", id,assignee , formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> deleteAttachment(String id, String attachmentID){

        String query = String.format("{ \"query\": \" mutation deleteAttachment {  deleteAttachment(input: {taskId: \\\"%s\\\", attachmentId: \\\"%s\\\"})} \" }", id, attachmentID);

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> updateAttachment(String id, String attachmentID, String attachmentUpdateInfo, TaskProp... properties) {

        String query = String.format("{ \"query\": \" mutation updateAttachment {  updateAttachment(input: {taskId: \\\"%s\\\", attachmentId: \\\"%s\\\", %s}}) {    attachment {      %s } }  }\" }", id, attachmentID, attachmentUpdateInfo, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> forwardTaskWithComment(String id,String assignee, String comment, TaskProp... properties) {
        String query = String.format("{ \"query\": \" mutation forwardTask {  forwardTask(input: {taskId: \\\"%s\\\", assignee: %s, comment: \\\"%s\\\"}) {    task{      %s } }  }\" }", id, assignee , comment, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return jsonNodeHttpResponse;
    }

    public HttpResponse<JsonNode> createComment(String id, String comment, TaskProp... properties) {

        String query = String.format("{ \"query\": \" mutation createComment {  createComment(input: {taskId: \\\"%s\\\", %s  }}}) {  %s }  }\" }", id, comment, formatTaskPropToString(properties));

        HttpResponse<JsonNode> jsonNodeHttpResponse = null;
        try {
            jsonNodeHttpResponse = Unirest.post(url).header("Content-Type", "application/json")
                    .body(query).asJson();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return jsonNodeHttpResponse;
    }
}

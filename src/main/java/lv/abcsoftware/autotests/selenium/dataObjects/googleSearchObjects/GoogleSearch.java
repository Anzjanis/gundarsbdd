package lv.abcsoftware.autotests.selenium.dataObjects.googleSearchObjects;


import lv.abcsoftware.autotests.selenium.model.BrowserInfo;
import lv.abcsoftware.autotests.selenium.util.ElementUtil;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;


public class GoogleSearch implements  IGoogleSearch{

    private WebDriver driver;
    private ElementUtil eu;

    public GoogleSearch(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        this.eu = new ElementUtil(driver);
    }


    SoftAssert softAssertion= new SoftAssert();

    @FindBy(how = How.ID, using = "lst-ib")
    static WebElement searchField;

    @FindBy(how = How.XPATH, using = "//*[@id=\"tsf\"]/div[2]/div[3]/center/input[1]")
    static WebElement googleSearchButton;

    @FindBy(how = How.ID, using = "search")
    static WebElement searchResults;

    @FindBy(how = How.ID, using = "resultStats")
    static WebElement searchResultName;



    public void checkIfBrowserInfoIsCorrect(BrowserInfo bo) {
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();

        softAssertion.assertTrue(driver.getCurrentUrl().contains(bo.getBaseURL()), "Current URL" + driver.getCurrentUrl() + "don't contain given URL" + bo.getBaseURL());
        softAssertion.assertTrue(browserName.equals(bo.getBrowserName().toLowerCase()), "Wrong browser: " + browserName + bo.getBrowserName().toLowerCase());
        softAssertion.assertTrue(bo.getLanguage().equals("LV"), "Language don't match: " + bo.getLanguage());
        softAssertion.assertAll();
    }

    public void checkIfMessageIsVisible(String arg0) {
        Assert.assertEquals(googleSearchButton.getAttribute("value"), arg0);
    }

    public void fillSearchField(String arg0) {

        searchField.sendKeys(arg0);
    }

    public void clickSearchButton() {

        googleSearchButton.click();
    }

    public void iSeeSearchResult() {

        Assert.assertTrue(searchResults.isDisplayed(), "Meklēšanas rezultāti neparādījās");

    }

    public void resultPage(String arg0) {
        Assert.assertTrue(searchResultName.getText().contains(arg0));
    }

    public void checkIfSearchNameIsCorrect(String arg0) {
        Assert.assertEquals(searchField.getAttribute("value"), arg0);
    }
}

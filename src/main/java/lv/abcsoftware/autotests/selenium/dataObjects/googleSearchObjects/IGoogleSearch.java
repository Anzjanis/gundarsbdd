package lv.abcsoftware.autotests.selenium.dataObjects.googleSearchObjects;

import lv.abcsoftware.autotests.selenium.model.BrowserInfo;

public interface IGoogleSearch {

    void checkIfBrowserInfoIsCorrect(BrowserInfo bo);

    void checkIfMessageIsVisible(String arg0);

    void fillSearchField(String arg0);

    void clickSearchButton();

    void resultPage(String arg0);

    void checkIfSearchNameIsCorrect(String arg0);

    void iSeeSearchResult();
}

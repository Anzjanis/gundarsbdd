package lv.abcsoftware.autotests.selenium.util;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;

public class ElementUtil {

    private WebDriver driver;
    public ElementUtil(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement fluientWaitforElement(WebElement element, Duration timeoutSec, Duration pollingSec) {

        FluentWait<WebDriver> fWait = new FluentWait<>(driver).withTimeout(timeoutSec)
                .pollingEvery(pollingSec)
                .ignoring(NoSuchElementException.class, TimeoutException.class).ignoring(StaleElementReferenceException.class);

        for (int i = 0; i < 2; i++) {
            try {
                     fWait.until(ExpectedConditions.visibilityOf(element));
                fWait.until(ExpectedConditions.elementToBeClickable(element));
            } catch (Exception e) {

                System.out.println("Element Not found trying again - " + element.toString().substring(70));
                e.printStackTrace();
            }
        }

        return element;

    }

}

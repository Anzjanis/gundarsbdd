package lv.abcsoftware.autotests.selenium.util;

import cucumber.api.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import lv.abcsoftware.autotests.selenium.seleniumConfiguration.SeleniumWebDriverConfiguration;

public class ScreenShotUtil {

    public static void takeScreeShot(Scenario scenario) {
            try {
                final byte[] screenshot = ((TakesScreenshot) SeleniumWebDriverConfiguration.getDriver())
                        .getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png"); //stick it in the report

            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}

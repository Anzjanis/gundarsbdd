package lv.abcsoftware.autotests.selenium.seleniumConfiguration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;

public class SeleniumWebDriverConfiguration {


    private static WebDriver driver;

    public static WebDriver getDriver() {
        return driver;
    }

    public static void setDriver(String browser) {
        ChromeOptions chromeOptions = new ChromeOptions();
        FirefoxOptions firefoxOptions = new FirefoxOptions();

        switch (browser) {
            case "Firefox":
               // firefoxOptions.addArguments("--start-maximized");
                SeleniumWebDriverConfiguration.driver = new FirefoxDriver(firefoxOptions);
                SeleniumWebDriverConfiguration.driver.manage().window().maximize();
                break;
            case "Chrome":
                chromeOptions.addArguments("--start-maximized");
                SeleniumWebDriverConfiguration.driver = new ChromeDriver(chromeOptions);

                break;
            default:
                chromeOptions.addArguments("--start-maximized");
                SeleniumWebDriverConfiguration.driver = new ChromeDriver(chromeOptions);

                break;
        }

        SeleniumWebDriverConfiguration.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        SeleniumWebDriverConfiguration.driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        SeleniumWebDriverConfiguration.driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
    }
}

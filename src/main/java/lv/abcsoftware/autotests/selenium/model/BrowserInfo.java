package lv.abcsoftware.autotests.selenium.model;

public class BrowserInfo {

    public BrowserInfo(String browserName, String baseURL, String language)
    {
        this.browserName = browserName;
        this.baseURL = baseURL;
        this.language = language;
    }

    public BrowserInfo()
    {

    }

    private String browserName;
    private String baseURL;
    private String language;

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String url) {
        this.baseURL = url;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}

package lv.abcsoftware.autotests.selenium.model;

import lv.abcsoftware.autotests.selenium.dataObjects.googleSearchObjects.GoogleSearch;
import lv.abcsoftware.autotests.selenium.dataObjects.googleSearchObjects.IGoogleSearch;
import org.openqa.selenium.WebDriver;
import lv.abcsoftware.autotests.selenium.seleniumConfiguration.SeleniumWebDriverConfiguration;


/*
    Klase kura saturēs visu nepieciešamo informāciju, lai testi varētu darboties, piemēram, informācija par driver vai
    datu kopas.
*/

public class BaseTestSelenium {

    protected WebDriver driver = SeleniumWebDriverConfiguration.getDriver();
    protected IGoogleSearch google = new GoogleSearch(driver);
    protected BrowserInfo browserInfo;
    public static final String url = System.getProperty("url");
}

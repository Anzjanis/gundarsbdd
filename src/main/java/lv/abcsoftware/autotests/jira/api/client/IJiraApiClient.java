package lv.abcsoftware.autotests.jira.api.client;

import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.List;
import java.util.Map;

public interface IJiraApiClient {

    void setJiraLabel(String issueNumber, String failed) throws UnirestException;

    void setJiraBody(Map<String,List<String>> dataMap, List<String> passed) throws UnirestException;
}

package lv.abcsoftware.autotests.jira.api.client;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.List;
import java.util.Map;

public class JiraApiClient implements IJiraApiClient {

    private static final String jiraURL = "https://gundarsdevelopment.atlassian.net";

    public void setJiraLabel(String issueNumber, String status) throws UnirestException {

        if(!(issueNumber.matches(".*\\d+.*"))){
            throw new IllegalArgumentException("IssueNumber nesatur skaitli, iespējams feature failā ir norādīts nekorekts tag: " + issueNumber);
        }

        if(status.equals("Failed")) {
            HttpResponse<String> abc = Unirest.put(jiraURL + "/rest/api/2/issue/" + issueNumber.replace("@", "") + "").basicAuth("Gundars.Anzjanis@gmail.com", "Parolite1")
                    .header("Content-Type", "application/json")
                    .body("{ \"update\": { \"labels\": [ {\"remove\" : \"Passed\"} ] } }").asString();
        } else {
            HttpResponse<String> abc = Unirest.put(jiraURL + "/rest/api/2/issue/" + issueNumber.replace("@", "") + "").basicAuth("Gundars.Anzjanis@gmail.com", "Parolite1")
                    .header("Content-Type", "application/json")
                    .body("{ \"update\": { \"labels\": [ {\"remove\" : \"Failed\"} ] } }").asString();
        }

        HttpResponse<String> abc = Unirest.put(jiraURL + "/rest/api/2/issue/" + issueNumber.replace("@", "") + "").basicAuth("Gundars.Anzjanis@gmail.com", "Parolite1")
                .header("Content-Type", "application/json")
                .body("{ \"update\": { \"labels\": [ {\"add\" : \" " + status + " \"} ] } }").asString();

    }

    public void setJiraBody(Map<String, List<String>> errorInfo, List<String> passed) throws UnirestException {
      for(String a : passed)
      {
          if(!(errorInfo.containsKey(a)))
          {
              setJiraLabel(a, "Passed");

              HttpResponse<String> abc = Unirest.put(jiraURL + "/rest/api/2/issue/"+a.replace("@", "")+"").basicAuth("Gundars.Anzjanis@gmail.com","Parolite1")
                      .header("Content-Type", "application/json")
                      .body("{ \"update\": { \"description\": [ {\"set\" : \"  *PASSED*   \"} ] } }").asString();
          }
      }

        for ( String key : errorInfo.keySet() ) {
            String body = createJiraIssueBody(errorInfo, key);

                HttpResponse<String> abc = Unirest.put(jiraURL + "/rest/api/2/issue/"+key.replace("@", "")+"").basicAuth("Gundars.Anzjanis@gmail.com","Parolite1")
                .header("Content-Type", "application/json")
                .body("{ \"update\": { \"description\": [ {\"set\" : \"  "+body+"   \"} ] } }").asString();
        }
    }

    private String createJiraIssueBody(Map<String, List<String>> errorInfo, String key) {
        String value = "*Failed scenarios:*" + "\\n";
        List<String> issueList = errorInfo.get(key);

        for (String error : issueList
             ) {
            value += "* " + error + "\\n";
        }
        return value;
    }
}
